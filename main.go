package main

import (
	jsoniter "github.com/json-iterator/go"
	"github.com/spf13/viper"
	"gitlab.com/csrnch/mqtt-postgresql-bridge/appconfig"
	"log"
	"os"
	"os/signal"
	"syscall"
)

var appCtx *appconfig.ApplicationContext

type BrokerMessage struct {
	job     *appconfig.ImportJob
	topic   string
	message []byte
}

func initializeApplicationContext() {
	var appContext appconfig.ApplicationContext
	appCtx = &appContext
	appCtx.JsonApi = jsoniter.ConfigCompatibleWithStandardLibrary
}

func main() {
	initializeApplicationContext()
	readApplicationConfig()

	log.Printf("Found %d import jobs in configuration, bridgeID:%s", len(appCtx.AppConfig.Jobs), appCtx.AppConfig.BridgeId)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	openDatabaseConnections()
	executeDDL()

	startListenMqtt()
	updatePublicGateways()

	defer closeAllDbConns()
	defer disconnectAllMqttClients()

	<-c
	log.Print("Shutdown by syscall.SIGTERM")
}

func readApplicationConfig() {
	viper.SetConfigName("mpb")
	viper.SetConfigType("json")
	viper.AddConfigPath("/etc/mqttpostgresqlbridge/")
	viper.AddConfigPath("$HOME/.mqttpostgresqlbridge")
	viper.AddConfigPath("./configs")

	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		log.Panicf("fatal error: %s", err)
	}
	viper.Unmarshal(&appCtx.AppConfig)
}
