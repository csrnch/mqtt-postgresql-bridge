package appconfig

import (
	"database/sql"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	jsoniter "github.com/json-iterator/go"
)

type ImportJob struct {
	Decoder               string
	MqttUser              string
	MqttPassword          string
	MqttHost              string
	MqttPort              int
	MqttTopic             string
	DbUser                string
	DbPassword            string
	DbHost                string
	DbSchema              string
	DbName                string
	DbPort                int
	Id                    string
	MqttClient            mqtt.Client
	DbConnection          *sql.DB
	TtnGtwBaseDataUpdates []TtnGtwBaseDataUpdate
}

type TtnGtwBaseDataUpdate struct {
	Url string
}

type AppConfiguration struct {
	BridgeId      string
	ListenAddress string
	ListenPort    int
	Jobs          []ImportJob `mapstructure:"jobs"`
}

type ApplicationContext struct {
	JsonApi   jsoniter.API
	AppConfig AppConfiguration
}
