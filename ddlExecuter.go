package main

import "log"

func executeDDL() {

	stmtCreateTableValues := `create table ttn_mapper_values
(
    id bigserial not null,
    bridge_id text,
    app_id text,
    dev_id text,
    hardware_serial text,
    altitude integer,
    latitude numeric,
    longitude numeric,
    hdop numeric,
    geohash text,
    port integer,
    counter bigint,
    payload_raw text,
    ttn_time timestamp with time zone NOT NULL,
    frequency numeric,
    modulation text,
    data_rate text,
    airtime numeric,
    coding_rate text,
    frame text,
    PRIMARY KEY (ID)
);`

	stmtCreateIndexValues := `CREATE INDEX time_idx
    ON ttn_mapper_values USING btree
    (ttn_time DESC NULLS LAST)`

	stmtCreateTableReports := `CREATE TABLE ttn_mapper_reports
(
    id bigserial NOT NULL,
    bridge_id text,
    frame text NOT NULL,
    gtw_id text,
    timestamp bigint,
    time timestamp with time zone,
    channel integer,
    rssi integer,
    snr numeric,
    rf_chain integer,
    latitude numeric,
    longitude numeric,
    geohash text,
    altitude integer,
    PRIMARY KEY (id)
)`

	stmtCreateindexReports := `CREATE INDEX frame_idx
    ON ttn_mapper_reports USING btree
    (frame ASC NULLS LAST)
;`

	stmtCreateTableTtnGtw := `CREATE TABLE ttn_gtw_basedata
(
    id text NOT NULL,
    description text,
    owner text,
    last_import timestamp with time zone,
    country_code text,
    brand text,
    frequencyplan text,
    placement text,
    latitude numeric,
    longitude numeric,
    altitude numeric,
    geohash text,
    PRIMARY KEY (id)
)`

	statements := []string{stmtCreateTableValues, stmtCreateTableReports, stmtCreateIndexValues, stmtCreateindexReports, stmtCreateTableTtnGtw}

	for jobIdx, _ := range appCtx.AppConfig.Jobs {

		jobP := appCtx.AppConfig.Jobs[jobIdx]

		for _, stmt := range statements {
			_, err := jobP.DbConnection.Exec(stmt)
			if err != nil {
				log.Printf("DDL ERROR %s", err)
			}
		}
	}
}
