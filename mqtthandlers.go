package main

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/google/uuid"
	"gitlab.com/csrnch/mqtt-postgresql-bridge/appconfig"
	"log"
	"strings"
	"time"
)

func startListenMqtt() {
	for idx, _ := range appCtx.AppConfig.Jobs {
		log.Printf("Starting Job '%s'", appCtx.AppConfig.Jobs[idx].Id)
		listenAndDispatch(&appCtx.AppConfig.Jobs[idx])
	}
}

func newMqttClient(job *appconfig.ImportJob) {
	opts := createClientOptions(job)
	job.MqttClient = mqtt.NewClient(opts)

	token := job.MqttClient.Connect()

	for !token.WaitTimeout(3 * time.Second) {
	}

	if err := token.Error(); err != nil {
		log.Printf("could not connect MQTT client for job: %s", job.Id)
		log.Fatal(err)
	}

	log.Printf("created mqtt client for job: %s", job.Id)
}

func createClientOptions(job *appconfig.ImportJob) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%d", job.MqttHost, job.MqttPort))

	if len(job.MqttUser) > 0 {
		opts.SetUsername(job.MqttUser)
		opts.SetPassword(job.MqttPassword)
	}

	opts.SetResumeSubs(false)
	opts.SetCleanSession(true)
	opts.SetOnConnectHandler(onConnectHandler)
	opts.SetConnectionLostHandler(connectionLostHandler)
	opts.SetClientID(uuid.New().String())
	return opts
}

func connectionLostHandler(client mqtt.Client, err error) {
	for idx, _ := range appCtx.AppConfig.Jobs {
		var job = appCtx.AppConfig.Jobs[idx]
		if job.MqttClient == client {
			log.Printf("Lost MQTT client connection for job %s", job.Id)
		}
	}
}

func onConnectHandler(client mqtt.Client) {
	for idx, _ := range appCtx.AppConfig.Jobs {
		var job = appCtx.AppConfig.Jobs[idx]
		if job.MqttClient == client {
			log.Printf("Connected MQTT client connection for job %s", job.Id)
			subscribeJobMqttClient(job)
		}
	}
}

func subscribeJobMqttClient(job appconfig.ImportJob) mqtt.Token {
	return job.MqttClient.Subscribe(job.MqttTopic, 0, func(client mqtt.Client, msg mqtt.Message) {

		message := BrokerMessage{job: &job, message: msg.Payload(), topic: msg.Topic()}

		switch strings.TrimSpace(strings.ToLower(job.Decoder)) {
		case "dev-fxq":
			go decodeFxqMessage(&message)
		default:
			log.Printf("NO DECODER FOUND Job: %s | [%s] %s\n", job.Id, msg.Topic(), string(msg.Payload()))
		}
	})
}

func listenAndDispatch(job *appconfig.ImportJob) {
	newMqttClient(job)
	log.Printf("Dispatching subscription for job %s", job.Id)
}

func disconnectAllMqttClients() {
	for _, job := range appCtx.AppConfig.Jobs {
		log.Printf("Disconnecting MQTT client for job: %s", job.Id)
		job.MqttClient.Disconnect(0)
		log.Print("...closed")
	}
}
