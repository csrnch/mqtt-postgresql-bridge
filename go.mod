module gitlab.com/csrnch/mqtt-postgresql-bridge

go 1.13

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/google/uuid v1.1.1
	github.com/json-iterator/go v1.1.6
	github.com/lib/pq v1.5.2
	github.com/mmcloughlin/geohash v0.9.0
	github.com/spf13/viper v1.7.0
)
