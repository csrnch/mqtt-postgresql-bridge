package main

import (
	"encoding/json"
	"github.com/mmcloughlin/geohash"
	"gitlab.com/csrnch/mqtt-postgresql-bridge/appconfig"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type TtnGtwBaseData struct {
	ID          string   `json:"id"`
	Description string   `json:"description"`
	Owner       string   `json:"owner"`
	Owners      []string `json:"owners"`
	Location    struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
		Altitude  float64 `json:"altitude"`
	} `json:"location"`
	CountryCode string `json:"country_code"`
	Attributes  struct {
		Brand         string `json:"brand"`
		FrequencyPlan string `json:"frequency_plan"`
		Placement     string `json:"placement"`
	} `json:"attributes"`
	LastSeen time.Time `json:"last_seen"`
}

func InsertOrUpdateGateways(gateways map[string]TtnGtwBaseData, importjob *appconfig.ImportJob) {

	var existing = map[string]bool{}

	sqlExistingGtws := `select id from ttn_gtw_basedata`
	rows, err := importjob.DbConnection.Query(sqlExistingGtws)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		id := ""
		rows.Scan(&id)
		existing[id] = true

	}

	rows.Close()

	for gtwId, gtw := range gateways {
		if gtw.Location.Latitude > 0 {
			if existing[gtwId] {
				updateGateway(gtw, importjob)
			} else {
				insertGateway(gtw, importjob)
			}

		}
	}
}

func insertGateway(gtw TtnGtwBaseData, i *appconfig.ImportJob) {

	insertNewGatewaySql := `insert into ttn_gtw_basedata(
	id, description, owner, last_import, country_code, brand, frequencyplan, placement, latitude, longitude, altitude, geohash)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);`

	log.Printf("inserting ttn gtw:%s", gtw.ID)
	_, err := i.DbConnection.Exec(insertNewGatewaySql, gtw.ID, gtw.Description, gtw.Owner, gtw.LastSeen, gtw.CountryCode, gtw.Attributes.Brand, gtw.Attributes.FrequencyPlan, gtw.Attributes.Placement, gtw.Location.Latitude, gtw.Location.Longitude, gtw.Location.Altitude, "")
	if err != nil {
		log.Print(err)
	}
}

func updateGateway(gtw TtnGtwBaseData, i *appconfig.ImportJob) {

	insertNewGatewaySql := `update ttn_gtw_basedata
	set description=$2, owner=$3, last_import=$4, country_code=$5, brand=$6, frequencyplan=$7, placement=$8, latitude=$9, longitude=$10, altitude=$11, geohash=$12
	where id =$1;`

	log.Printf("updating ttn gtw:%s", gtw.ID)
	_, err := i.DbConnection.Exec(insertNewGatewaySql, gtw.ID, gtw.Description, gtw.Owner, gtw.LastSeen, gtw.CountryCode, gtw.Attributes.Brand, gtw.Attributes.FrequencyPlan, gtw.Attributes.Placement, gtw.Location.Latitude, gtw.Location.Longitude, gtw.Location.Altitude, geohash.Encode(gtw.Location.Latitude, gtw.Location.Longitude))
	if err != nil {
		log.Print(err)
	}
}

func updatePublicGateways() {

	for jobIdx, _ := range appCtx.AppConfig.Jobs {

		jobP := appCtx.AppConfig.Jobs[jobIdx]

		if nil == jobP.TtnGtwBaseDataUpdates {
			continue
		}

		for baseUpdateIdx, _ := range jobP.TtnGtwBaseDataUpdates {
			baseDataUpdateObj := jobP.TtnGtwBaseDataUpdates[baseUpdateIdx]
			insertOrUpdateUrl(&jobP, baseDataUpdateObj.Url)
		}

	}
}

func insertOrUpdateUrl(importjob *appconfig.ImportJob, url string) {

	ttnHttpClient := http.Client{
		Timeout: time.Second * 2, // Maximum of 2 secs
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Print(err)
		return
	}

	res, err := ttnHttpClient.Do(req)

	if err != nil {
		log.Print(err)
		return
	}

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		log.Print(err)
		return
	}

	var gtwMap map[string]TtnGtwBaseData

	err = json.Unmarshal(body, &gtwMap)

	if err != nil {
		log.Print(err)
		return
	}

	InsertOrUpdateGateways(gtwMap, importjob)
}
