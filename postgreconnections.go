package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func openDatabaseConnections() {

	for idx, _ := range appCtx.AppConfig.Jobs {

		var job = &appCtx.AppConfig.Jobs[idx]

		db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s search_path=%s sslmode=require", job.DbHost, job.DbPort, job.DbName, job.DbUser, job.DbPassword, job.DbSchema))

		if err != nil {
			log.Printf("Could not open database connection for job %s", job.Id)
			log.Fatal(err)
		}

		err = db.Ping()
		if err != nil {
			log.Printf("Could not connect database for job %s", job.Id)
			log.Fatal(err)
		} else {
			job.DbConnection = db
			continue
		}
	}
}

func closeAllDbConns() {
	for idx, _ := range appCtx.AppConfig.Jobs {
		var job = &appCtx.AppConfig.Jobs[idx]

		log.Printf("Closing DB connection of job: %s", job.Id)
		err := job.DbConnection.Close()

		if err != nil {
			log.Panicf("could not close database conneciton: %s", err)
		}

		log.Print("...closed")
	}
}
