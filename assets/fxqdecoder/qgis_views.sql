create view qgis_ttn_report_lines as
select distinct fr.id,
fr.bridge_id,
fr.frame,
fr.gtw_id,
fr.timestamp,
fr.time,
fr.channel,
fr.rssi,
fr.snr,
fr.rf_chain,
fr.latitude,
fr.longitude,
fr.geohash,
fr.altitude,
st_setsrid(st_makeline(st_makepoint(tg.longitude, tg.latitude), st_makepoint(fv.longitude, fv.latitude)), 4326) as geom
from ttn_mapper_values fv,
ttn_mapper_reports fr,
ttn_gtw_basedata tg
where tg.id = fr.gtw_id and fv.frame = fr.frame and fr.bridge_id = 'prod'
order by fr.id desc;


create view qgis_ttn_mapper_values as
SELECT *,
st_setsrid(st_makepoint(longitude, latitude, altitude), 4326) AS geom
FROM ttn_mapper_values;

create view qgis_ttn_gtw_basedata as
SELECT *,
st_setsrid(st_makepoint(longitude, latitude, altitude), 4326) AS geom
FROM ttn_gtw_basedata;