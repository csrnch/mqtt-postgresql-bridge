package main

import (
	"github.com/google/uuid"
	"github.com/mmcloughlin/geohash"
	"log"
	"strings"
)

type fxqJsonTTN struct {
	AppID          string `json:"app_id"`
	DevID          string `json:"dev_id"`
	HardwareSerial string `json:"hardware_serial"`
	Port           int    `json:"port"`
	Counter        int    `json:"counter"`
	PayloadRaw     string `json:"payload_raw"`
	PayloadFields  struct {
		Altitude  int     `json:"altitude"`
		Hdop      float64 `json:"hdop"`
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"payload_fields"`
	Metadata struct {
		Time       string  `json:"time"` // use string, because TTN sometimes sending empty strings instead of valid time value
		Frequency  float64 `json:"frequency"`
		Modulation string  `json:"modulation"`
		DataRate   string  `json:"data_rate"`
		Airtime    int     `json:"airtime"`
		CodingRate string  `json:"coding_rate"`
		Gateways   []struct {
			GtwID      string  `json:"gtw_id"`
			Timestamp  int64   `json:"timestamp,omitempty"`
			Time       string  `json:"time,omitempty"` // use string, because TTN sometimes sending empty strings instead of valid time value
			Channel    int     `json:"channel,omitempty"`
			Rssi       int     `json:"rssi,omitempty"`
			Snr        float64 `json:"snr,omitempty"`
			RfChain    int     `json:"rf_chain,omitempty"`
			Latitude   float64 `json:"latitude,omitempty"`
			Longitude  float64 `json:"longitude,omitempty"`
			Altitude   int     `json:"altitude,omitempty"`
			GtwTrusted bool    `json:"gtw_trusted,omitempty"`
		} `json:"gateways"`
	} `json:"metadata"`
}

func decodeFxqMessage(brokerMessage *BrokerMessage) {
	log.Printf("FXQ DECODE NEW BROKER MESSAGE Job: %s, Topic: %s", brokerMessage.job.Id, brokerMessage.topic)

	data := fxqJsonTTN{}
	unmarshalError := appCtx.JsonApi.Unmarshal(brokerMessage.message, &data)

	if unmarshalError != nil {
		log.Print(unmarshalError)
		return
	}

	frame := uuid.New().String()

	lastInsertId := 0

	sqlStatement := `insert into ttn_mapper_values(
	bridge_id,
	app_id, 
	dev_id, 
	hardware_serial, 
	altitude, 
	latitude, 
	longitude, 
	hdop, 
	geohash, 
	port, 
	counter, 
	payload_raw, 
	ttn_time, 
	frequency, 
	modulation, 
	data_rate, 
	airtime, 
	coding_rate,
	frame)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19) RETURNING id;`

	err := brokerMessage.job.DbConnection.QueryRow(sqlStatement,
		appCtx.AppConfig.BridgeId,
		data.AppID,
		data.DevID,
		data.HardwareSerial,
		data.PayloadFields.Altitude,
		data.PayloadFields.Latitude,
		data.PayloadFields.Longitude,
		data.PayloadFields.Hdop,
		geohash.Encode(data.PayloadFields.Latitude, data.PayloadFields.Longitude),
		data.Port,
		data.Counter,
		data.PayloadRaw,
		data.Metadata.Time,
		data.Metadata.Frequency,
		data.Metadata.Modulation,
		data.Metadata.DataRate,
		data.Metadata.Airtime,
		data.Metadata.CodingRate,
		frame).Scan(&lastInsertId)

	if err != nil {
		log.Print(err)
	}

	log.Printf("Inserted values for frame %s with  ID: %d DevID: %s", frame, lastInsertId, data.DevID)

	for _, gateway := range data.Metadata.Gateways {

		if len(strings.TrimSpace(gateway.Time)) == 0 {
			gateway.Time = data.Metadata.Time
		}

		sqlStatement = `insert into ttn_mapper_reports(
	bridge_id,
	frame, 
	gtw_id, 
	timestamp, 
	time, 
	channel, 
	rssi, 
	snr, 
	rf_chain, 
	latitude, 
	longitude, 
	geohash, 
	altitude)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING id;`

		err = brokerMessage.job.DbConnection.QueryRow(sqlStatement,
			appCtx.AppConfig.BridgeId,
			frame,
			gateway.GtwID,
			gateway.Timestamp,
			gateway.Time,
			gateway.Channel,
			gateway.Rssi,
			gateway.Snr,
			gateway.RfChain,
			gateway.Latitude,
			gateway.Longitude,
			geohash.Encode(gateway.Latitude, gateway.Longitude),
			gateway.Altitude).Scan(&lastInsertId)

		if err != nil {
			log.Print(err)
		}

		if gateway.Latitude == 0 && gateway.Longitude == 0 {
			log.Printf("DEBUG: NULL LOCATION IN MESSAGE %s", brokerMessage.message)
		}

		log.Printf("Inserted reports for frame %s with  ID: %d GtwID: %s", frame, lastInsertId, gateway.GtwID)
	}
}
